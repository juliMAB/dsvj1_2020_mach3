#ifndef MENU
#define MENU
#include "scenes/menu/buton.h"
#include "scenes/menu/generalities.h"
#include <iostream>
class menu
{
public:
	menu(Texture2D texture);
	~menu();
	void updateMusic();
	void update();
	void drawMenu();

private:
	Texture2D _texture; //se entiende de sobra que es el fondo.
	Texture2D _textureButons;
	buton* _buton[cuantitiesButons];
};
#endif // !MENU
