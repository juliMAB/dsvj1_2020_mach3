#ifndef GAMEPLAY
#define GAMEPLAY
#include "raylib.h"
#include "scenes/gameplay/modulos.h"
class gameplay
{
public:
	gameplay();
	~gameplay();
	void draw();
	void update();
	void musicUpdate();
private:
	modulo_pvE*		  pvE;
	modulo_matrix* matrix;
	Music music;
};

#endif // !GAMEPLAY
