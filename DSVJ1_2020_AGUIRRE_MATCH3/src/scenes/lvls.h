#ifndef LVLS
#define LVLS
#include "raylib.h"
#include "scenes/menu/generalities.h"
#include "menu/buton.h"
#include <string>
class lvls
{
public:
	lvls(Texture2D texture);
	void update();
	void draw();
	~lvls();

private:
	Texture2D _texture;
	Texture2D _butonTexture;
	Texture2D _butonBTexture;
	Texture2D _butonSTexture;
	buton* _butons[levelsMax];
	buton* _butonB;
	buton* _butonS;
};

#endif // !LVLS
