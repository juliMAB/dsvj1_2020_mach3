#ifndef BUTON
#define BUTON
#include "raylib.h"
#include "scenes/menu/generalities.h"
#include <string>
class buton
{
public:
	buton(short buton, Vector2 pos, Texture2D texture,short cuantity);
	buton(short buton, Vector2 pos, Texture2D texture,short cuantity,std::string text);
	bool actualizar();
	void drawButon(Texture2D texture);
	~buton();

private:
	Vector2			_pos; //pivot en centro.
	Rectangle		_rec; //rect en pantalla.
	float   _frameHeight; //alto del frame.
	float    _frameWidth; //ancho del frame.
	Rectangle	 _inside; //rec interno del texture.
	short		  _state; //estado.
	std::string    _text; //que dice.
};
#endif
