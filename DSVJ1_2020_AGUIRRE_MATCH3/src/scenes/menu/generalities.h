#ifndef GENERALITIES
#define GENERALITIES
#include <string>
#include "raylib.h"

//LoadMusicStream("../res/assets/MusicaMenu.mp4")
// y aca vamos a tirar todos los magic numbers.
const int cuantitiesButons = 4;
const int levelsMax = 5;
//const int lettersSmall = GetScreenHeight()/100;
enum class butons
{
	play,
	help,
	credits,
	close
};
enum class Scene
{
	Menu,
	Game,
	Help,
	Credits,
	lvls,
	shop
};
enum class Reader
{
	unlookLevel,
	size
	
};

namespace control
{
	namespace screen
	{
		extern int height;
		extern int width;
	}
	extern bool gameOver;
	extern Scene currentScene;
	extern Scene newCurrentScene;
	extern float volumeFx;
	extern float volumeMusic;
}
namespace gameplayLogic
{
	extern int UnlookLevel;
	extern int currentLevel;
}
namespace letters
{
	extern int small;
	extern int medium;
	extern int big;
}

#endif // !GENERALITIES
