#include "buton.h"

buton::buton(short buton, Vector2 pos, Texture2D texture, short cuantity)
{
	_frameHeight = static_cast<float>(texture.height / 2); //cantidad de frames.
	_frameWidth = static_cast<float>(texture.width / cuantity);
	_state = 0;
	_inside = { _frameWidth*buton,_frameHeight*_state,_frameWidth ,_frameHeight };
	_rec = { pos.x-_frameWidth/2,pos.y-_frameHeight/2,_frameWidth,_frameHeight };
	
	_pos = pos;
	_text = " ";
}

buton::buton(short buton, Vector2 pos, Texture2D texture,short cuantity, std::string text)
{
	_frameHeight = static_cast<float>(texture.height / 2); //cantidad de frames.
	_frameWidth = static_cast<float>(texture.width / cuantity);
	_inside = { _frameWidth * buton,_frameHeight,_frameWidth ,_frameHeight };
	_rec = { pos.x - _frameWidth / 2,pos.y - _frameHeight / 2,_frameWidth,_frameHeight };
	_state = 0;
	_pos = pos;
	_text = text;
}


bool buton::actualizar()
{
	Vector2 mousePoint = GetMousePosition();
	if (CheckCollisionPointRec(mousePoint, _rec))
	{
		_state = 1;
		_inside.y = _state * _frameHeight;
		if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
		{
			return true;
		}
		return false;
	}
	else
	{
		_state = 0;
		_inside.y = _state * _frameHeight;
		return false;
		
	}
	
}


void buton::drawButon(Texture2D texture)
{
	DrawTextureRec(texture, _inside, { _pos.x - _frameWidth / 2,_pos.y - _frameHeight / 2 }, WHITE);
	DrawText(&_text[0], _pos.x-MeasureText("0",letters::small)/2, _pos.y-letters::small, 5, WHITE);
}

buton::~buton()
{ }
