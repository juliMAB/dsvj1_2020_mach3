#include "lvls.h"

lvls::lvls(Texture2D texture)
{
	//coloco fondo
	_texture = texture;
	_butonTexture = LoadTexture("res/assets/botonN.png");
	_butonTexture.width = static_cast<float>(static_cast<float>(GetScreenWidth()) / 20);
	_butonTexture.height = static_cast<float>(static_cast<float>(GetScreenWidth()) / 20);
	short separationB = 10;
	short separationA = 15;
	Vector2 colocator = { static_cast<float>(_texture.width/10*1),static_cast<float>(_texture.height / 10 * 1) };
	Vector2 PosButons[levelsMax];
	for (int i = 0; i < levelsMax; i++)
	{
		PosButons[i] = colocator;
		colocator.x += separationB + _butonTexture.width;
		if (colocator.x> _texture.width / 10 * 9)
		{
			colocator.y += separationA;
			colocator.x = static_cast<float>(_texture.width / 10 * 1);
		}
	}
	for (int i = 0; i < levelsMax; i++)
	{
		_butons[i] = new buton(0, PosButons[i], _butonTexture, 1, FormatText("%01i", i));
	}
	_butonBTexture = LoadTexture("res/assets/botonBack.png");
	_butonSTexture = LoadTexture("res/assets/botonShop.png");
	_butonBTexture.width = static_cast<float>(static_cast<float>(GetScreenWidth()) / 6);
	_butonBTexture.height = static_cast<float>(static_cast<float>(GetScreenWidth()) / 10);
	_butonSTexture.width = static_cast<float>(static_cast<float>(GetScreenWidth()) / 6);
	_butonSTexture.height = static_cast<float>(static_cast<float>(GetScreenWidth()) / 10);
	colocator = { static_cast<float>(GetScreenWidth()/10*1),static_cast<float>(GetScreenHeight()/10*9)};
	_butonB = new buton(0, colocator, _butonBTexture, 1);
	colocator = { static_cast<float>(GetScreenWidth() / 10 * 2),static_cast<float>(GetScreenHeight() / 10 * 6) };
	_butonS = new buton(0, colocator, _butonSTexture, 1);
	//falta completar la creacion de los botones shop
}
void lvls::update()
{
	for (int i = 0; i < levelsMax; i++)
	{
		if (_butons[i]->actualizar())
			if (gameplayLogic::UnlookLevel>=i)
			{ 
				control::newCurrentScene = Scene::Game;
				gameplayLogic::currentLevel = i;
			}	
	}
	if (_butonB->actualizar())
		control::newCurrentScene = Scene::Menu;
	if (_butonS->actualizar())
		control::newCurrentScene = Scene::shop;
}
void lvls::draw()
{
	_texture.height = GetScreenHeight();
	_texture.width = GetScreenWidth();
	DrawTexture(_texture, 0, 0, WHITE);

	for (int i = 0; i < levelsMax; i++)
	{
		_butons[i]->drawButon(_butonTexture);
	}
	_butonB->drawButon(_butonBTexture);
	_butonS->drawButon(_butonSTexture);
}
lvls::~lvls()
{
	UnloadTexture(_texture);
	UnloadTexture(_butonBTexture);
	UnloadTexture(_butonSTexture);
	UnloadTexture(_butonTexture);
	for (int i = 0; i < levelsMax; i++)
	{
		_butons[i]->~buton();
	}
	delete _butonB;
	delete _butonS;
}
