#include "gameplay.h"

gameplay::gameplay()
{
	pvE = new modulo_pvE();
	matrix = new modulo_matrix();
	music = LoadMusicStream("res/assets/beat2.mp3");
	SetMusicVolume(music, 0.5f);
}

gameplay::~gameplay()
{
	delete matrix;
	delete pvE;
	UnloadMusicStream(music);
}

void gameplay::draw()
{
	matrix->draw();
	pvE->draw();
	matrix->draw(pvE->gameOver());
}

void gameplay::update()
{
	matrix->update();
	pvE->update();
	musicUpdate();
	
}

void gameplay::musicUpdate()
{
	if (!IsMusicPlaying(music))
	{
		PlayMusicStream(music);
	}
	else
	{
		UpdateMusicStream(music);
	}
}

