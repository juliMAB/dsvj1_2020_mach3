#ifndef HELP
#define HELP
#include "raylib.h"
#include "menu/buton.h"
#include "menu/generalities.h"
class help
{
public:
	help(Texture2D textura);
	void update();
	void draw();
	~help();
private:
	Texture2D    _texture;
	buton*			backB;
	Texture2D	  TxBackB;
};
#endif // !HELP
