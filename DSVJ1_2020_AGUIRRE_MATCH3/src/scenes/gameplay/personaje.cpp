#include "personaje.h"

personaje::personaje(int i,Rectangle rec)
{
	_texture = LoadTexture("res/assets/stickMan.png");
	_texture.width = GetScreenWidth() / 4;
	_texture.height = GetScreenHeight() / 2;
	_action = doing::breathing;
	_timer = 0;
	_alive = true;
	_timeToAtack = 0;
	_frame = 0;
	_protection = 0;
	_damage = 3;
	_mana = 4;
	_life = 10;
	_resistence = 0;
	_debuf = 0;
	_buf = 0;
	_type = static_cast<damage>(i);
	width = _texture.width / 5;
	height = _texture.height / 5;
	_inside = { static_cast<float>(static_cast<int>(_type) * width),0,static_cast<float>(width),static_cast<float>(height) };
	_rec = { 0,0,0,0 };
	switch (static_cast<damage>(i))
	{
	case damage::None:
		_rec = { 0,0,0,0 };
		break;
	case damage::sword:
		_rec = { static_cast<float>(width + 8),-static_cast<float>(height*2 + 4),static_cast<float>(width),static_cast<float>(height) };
		break;
	case damage::magic:
		_rec = { static_cast<float>(width + 8),-static_cast<float>(height + 2),static_cast<float>(width),static_cast<float>(height) };
		break;
	case damage::bow:
		_rec = { 4 ,-static_cast<float>(height + height/2 + 4),static_cast<float>(width),static_cast<float>(height) };
		break;
	case damage::shield:
		_rec = { static_cast<float>(4 + width*2),-static_cast<float>(height + height / 2 + 4),static_cast<float>(width),static_cast<float>(height) };
		break;
	case damage::cuantitis:
		_rec = { 0,0,0,0 };
		break;
	default:
		_rec = { 0,0,0,0 };
		break;
	}
	_rec.x += rec.x;
	_rec.y += rec.y + rec.height;
	_recIni = _rec;
}
personaje::~personaje()
{
	UnloadTexture(_texture);
}
void personaje::draw()
{
	if (_alive)
	{
		DrawTextureRec(_texture, _inside, { _rec.x,_rec.y }, WHITE);
#if _DEBUG
		DrawRectangleLines(static_cast<int>(_rec.x), static_cast<int>(_rec.y), static_cast<int>(_rec.width), static_cast<int>(_rec.height), WHITE);
#endif // DEBUG
	}
	
}
void personaje::animation()
{
	switch (_action)
	{
	case doing::None:
		break;
	case doing::breathing:
		_timer--;
		if (_timer<0)
		{
			_frame = 1;
		}
		if (_timer<-20)
		{
			_timer = 20;
			_frame = 0;
		}
		if (_rec.x > _recIni.x)
		{
			_rec.x--;
		}
		break;
	case doing::attacking:
		_rec.x++;
		if (_timer < 0)
		{
			_frame = 2;
		}
		if (_timer < -20)
		{
			_timer = 20;
			_frame = 3;
		}
		if (_rec.x+_rec.width>=GetScreenWidth()/2)
		{
			_frame = 4;
			_timer = 180;
		}
		if (_frame == 4)
		{
			_action = doing::breathing;
		}
		break;
	case doing::dying:
		break;
	case doing::cuantitis:
		break;
	default:
		break;
	}
	_inside = { static_cast<float>(static_cast<int>(_type) * width),static_cast<float>(height*_frame),static_cast<float>(width),static_cast<float>(height) };
}
void personaje::update()
{
	if (_life<=0)
	{
		_alive = false;
	}
	else
	{
		animation();
	}
}
void personaje::atack(enemy* p)
{
	if (_alive)
	{
		_action = doing::attacking;
		p->setLife(p->getLife() - _damage);
	}
}
void personaje::setLife(int value)
{
	_life = value;
}
int personaje::getLife()
{
	return _life;
}

enemy::enemy(int i, Rectangle rec)
{
	_texture = LoadTexture("res/assets/stickMan.png");
	_texture.width = GetScreenWidth() / 4;
	_texture.height = GetScreenHeight() / 2;
	_action = doing::breathing;
	_timer = 0;
	_alive = true;
	_timeToAtack = 600;
	_frame = 0;
	_damage = 3;
	_life = 10;
	_type = static_cast<damage>(i);
	width = _texture.width / 5;
	height = _texture.height / 5;
	_inside = { static_cast<float>(static_cast<int>(_type) * width),0,static_cast<float>(width),static_cast<float>(height) };
	_rec = { static_cast<float>(-4-width) ,-static_cast<float>(height + height / 2 + 4),static_cast<float>(width),static_cast<float>(height) };
	_rec.x += rec.x+ rec.width;
	_rec.y += rec.y + rec.height;
	_recIni = _rec;
}
enemy::~enemy()
{
	UnloadTexture(_texture);
}
void enemy::animation()
{
	switch (_action)
	{
	case doing::None:
		break;
	case doing::breathing:
		_timer--;
		if (_timer < 0)
		{
			_frame = 1;
		}
		if (_timer < -30)
		{
			_timer = 60;
			_frame = 0;
		}
		if (_rec.x < _recIni.x)
		{
			_rec.x++;
		}
		break;
	case doing::attacking:
		_rec.x--;
		if (_timer < 0)
		{
			_frame = 2;
		}
		if (_timer < -30)
		{
			_timer = 60;
			_frame = 3;
		}
		if (_rec.x  <= GetScreenWidth() / 2)
		{
			_frame = 4;
			_timer = 120;
			_action = doing::breathing;
		}
		break;
	case doing::dying:
		break;
	case doing::cuantitis:
		break;
	default:
		break;
	}
	_inside = { static_cast<float>(static_cast<int>(_type) * width),static_cast<float>(_frame*height),static_cast<float>(width),static_cast<float>(height) };

}
void enemy::update()
{
	kill();
	if (_life <= 0)
	{
		_alive = false;
	}
	else
	{
		if (_timeToAtack <= 0)
		{
			
			_timeToAtack = 5000;
		}
		_timeToAtack--;
		
		animation();
	}
}
void enemy::setLife(int value)
{
	_life = value;
}
int enemy::getLife()
{
	return _life;
}
void enemy::kill()
{
	if (IsKeyPressed(KEY_K))
	{
		_life = 0;
		_alive = false;
	}
}
void enemy::atack(personaje* p)
{
	if (_alive)
	{
		if (_timeToAtack<=0)
		{
			p->setLife(p->getLife() - _damage);
			_action = doing::attacking;
		}
		
	}
}
void enemy::draw()
{
	if (_alive)
	{
		DrawTextureRec(_texture, _inside, { _rec.x,_rec.y }, WHITE);
#if _DEBUG
		DrawRectangleLines(static_cast<int>(_rec.x), static_cast<int>(_rec.y), static_cast<int>(_rec.width), static_cast<int>(_rec.height), WHITE);
#endif // DEBUG
	}
	

}