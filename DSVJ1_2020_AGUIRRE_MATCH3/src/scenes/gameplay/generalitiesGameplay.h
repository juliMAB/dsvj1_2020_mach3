#ifndef GENERALITIESGAMEPLAY
#define GENERALITIESGAMEPLAY
#include "raylib.h"
enum class gems
{
	None,
	Ruby,
	Shapire,
	Esmeralda,
	Amatista,
	cuantitis
};

enum class damage
{
	None,
	sword,
	magic,
	bow,
	shield,
	cuantitis
};
enum class doing
{
	None,
	breathing,
	attacking,
	dying,
	cuantitis
};
 
namespace cuantitis
{
	namespace matrix
	{
		extern gems diferentGems;
		int const columns = 9;
		int const rows = 9;
		int const totalGems = rows * columns;
	}
	namespace rpg
	{
		extern int enemigs;
		extern int allies;
		extern int damageType[static_cast<int>(damage::cuantitis)];
		extern int charge;
		extern int damageTypeTotal;
		extern int Enemigcharge;
	}
	namespace gem
	{
		extern int frames;
		extern int states;
	}
	namespace logic
	{
		extern int minSelect;
		extern int maxTurns;
	}
}
namespace timers
{
	extern int gemSpawns;
}
namespace letters
{
	//extern int small;
	//extern int medium;
	//extern int big;
}
struct Gem
{
	gems	 type;
	Rectangle rec; //pos.
	Color   color;
	Rectangle ins; //imagen interna
	int     frame;
	int		state;
	bool   select;
};
struct Int2
{
	int x;
	int y;
};
Color giveColor(gems g);
Color giveColorLight(gems g);
bool nextToF(Vector2 SelectedP,int i,int f);
#endif // !GENERALITIESGAMEPLAY
