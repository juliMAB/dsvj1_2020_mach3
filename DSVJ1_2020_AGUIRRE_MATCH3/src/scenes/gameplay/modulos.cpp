#include "modulos.h"

modulo_matrix::modulo_matrix()
{
	_TextureF = LoadTexture("res/assets/fin.png");
	_TextureF.width = GetScreenWidth() / 4;
	_TextureF.height = GetScreenHeight() / 4;
	SelectedP = { 0,0 };
	_falling = false;
	SelectedG = gems::None;
	breakBroak = LoadSound("res/assets/crack.wav");
	_textureG = LoadTexture("res/assets/Gema.png");
	_texture = LoadTexture("res/assets/matrix.png");
	_texture.height = static_cast<int>(GetScreenHeight() / 2);
	_texture.width = static_cast<int>( GetScreenWidth()  / 2);
	_rec =
	{ static_cast<float>(  GetScreenWidth()  / 2) - _texture.width / 2,
		static_cast<float>(GetScreenHeight() / 2),
		static_cast<float>(_texture.width),
		static_cast<float>(_texture.height)
	};
	_marco = _rec;
	_marco = { _marco.x + 10, _marco.y + 10, _marco.width - 20, _marco.height - 20 };

	_textureG.width = static_cast<int>(_marco.width) / cuantitis::matrix::columns;
	_textureG.height = static_cast<int>(_marco.height) / cuantitis::matrix::rows * cuantitis::gem::frames;
	gemHeight = static_cast<float>(_textureG.height)/cuantitis::gem::frames;
	gemWidth = static_cast<float>(_textureG.width);
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			_matrix[i][f].type = gems::None;
			_matrix[i][f].rec = { _marco.x + static_cast<float>(_textureG.width) * f,_marco.y + gemHeight * i,gemWidth,gemHeight };
			_matrix[i][f].color = { giveColor(_matrix[i][f].type) };
			_matrix[i][f].frame = 0;
			_matrix[i][f].state = 0;
			_matrix[i][f].ins = { 0,0,static_cast<float>(_textureG.width),gemHeight };
			_matrix[i][f].select = false;
		}
	}
}
modulo_matrix::~modulo_matrix()
{
	UnloadTexture(_textureG);
	UnloadTexture(_texture);
	UnloadSound(breakBroak);
	UnloadTexture(_TextureF);
}
void modulo_matrix::fallingGems()
{
	_falling = false;
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			if (!(_matrix[i][f].type == gems::None))
			{
				if (_matrix[i][f].rec.y < _marco.y + gemHeight * i)
				{
					_matrix[i][f].rec.y+=30;
					_falling = true;
				}
			}
		}
	}
}
void modulo_matrix::spawnGemsTop()
{
	if (timers::gemSpawns>0)
	{
		timers::gemSpawns-=5;
	}
	else
	{
		for (int i = 0; i < cuantitis::matrix::columns; i++)
		{
			//para la primer linea.
			if (_matrix[0][i].type == gems::None)
			{
				_matrix[0][i].type = static_cast<gems>(GetRandomValue(1, static_cast<int>(gems::cuantitis) - 1));
				_matrix[0][i].color = giveColor(_matrix[0][i].type);
				_matrix[0][i].rec.y -= _marco.y;
				if (timers::gemSpawns == 0)
				{
					timers::gemSpawns = 10;
					return;
				}
			}
			//para el resto. //si hay una matriz en null, toma el valor de arriba.
			for (int f = 0; f < cuantitis::matrix::rows; f++)
			{
				if (f!=0)
				{
					if (_matrix[f][i].type == gems::None)
					{
						_matrix[f][i] = _matrix[f - 1][i];
						_matrix[f - 1][i].type = gems::None;
					}
				}
			}	
		}
	}
}
void modulo_matrix::correctFalling()
{
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			if (!(_matrix[i][f].type == gems::None))
			{
				if (_matrix[i][f].rec.y > _marco.y + gemHeight * i)
				{
					_matrix[i][f].rec.y= _marco.y + gemHeight * i;
				}
			}
		}
	}
}
void modulo_matrix::update()
{
	spawnGemsTop();
	fallingGems();
	correctFalling();
	if (!_falling)
	{
		select();
		destroyBlocks();
	}
	else
	{
		_selected.clear();
	}
	inputs();
	updateGem();
}
void modulo_matrix::clearMatrix()
{
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			_matrix[i][f].type = gems::None;
		}
	}
}
void modulo_matrix::inputs()
{
	if (IsKeyPressed(KEY_R))
		clearMatrix();
}
void modulo_matrix::draw()
{
	//matrix marco
	//draw matrix
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			if ((_matrix[i][f].type == gems::None))
			{
#if DEBUG
				DrawText("NONE", static_cast<int>(_matrix[i][f].rec.x), static_cast<int>(_matrix[i][f].rec.y), 10, RED);
#endif
			}
			else
			{
				if (_selected.size()==0)
				{
					DrawTextureRec(_textureG, _matrix[i][f].ins, { _matrix[i][f].rec.x,_matrix[i][f].rec.y }, _matrix[i][f].color);
				}
				else
				{
					for (int j = 0; j < static_cast<int>(_selected.size()); j++)
					{
						if (_selected[j].x == _matrix[i][f].rec.x && _selected[j].y == _matrix[i][f].rec.y)
						{
							_matrix[i][f].color = giveColorLight(_matrix[i][f].type);
							DrawTextureRec(_textureG, _matrix[i][f].ins, { _matrix[i][f].rec.x,_matrix[i][f].rec.y }, _matrix[i][f].color);
						}
					}
					if (_matrix[i][f].color.r ==155)
					{
					}
					else
					{
						DrawTextureRec(_textureG, _matrix[i][f].ins, { _matrix[i][f].rec.x,_matrix[i][f].rec.y }, _matrix[i][f].color);
					}

				}
				
			}
#if DEBUG			
				DrawRectangleLines(static_cast<int>(_matrix[i][f].rec.x), static_cast<int>(_matrix[i][f].rec.y), static_cast<int>(_matrix[i][f].ins.width), static_cast<int>(_matrix[i][f].ins.height), GREEN);
				DrawText(FormatText("%i,%i", i, f, 10), static_cast<int>(_matrix[i][f].rec.x), static_cast<int>(_matrix[i][f].rec.y), 10, RED);
#endif
		}
	}
	DrawTextureRec(_texture, { 0,0,static_cast<float>(_texture.width),static_cast<float>(_texture.height) }, { _rec.x,_rec.y }, WHITE);
	DrawText(FormatText("Left turns: %i", cuantitis::logic::maxTurns), 0, 0,10, RED);
}
void modulo_matrix::draw(bool a)
{
	if (a)
	{
		DrawRectangle(static_cast<int>(_marco.x), static_cast<int>(_marco.y), static_cast<int>(_marco.width), static_cast<int>(_marco.height), BLACK);
		DrawText(FormatText("GameOver"), GetScreenWidth()/2 - MeasureText("GameOver", letters::big), static_cast<int>(_marco.height / 2 + _marco.height - letters::big), letters::big, RED);
		DrawTextureRec(_TextureF, { 0,0,static_cast<float>(_TextureF.width),static_cast<float>(_TextureF.height) }, { static_cast<float>(GetScreenWidth() / 2 - MeasureText("GameOver", letters::big)),static_cast<float>(_marco.height / 2 + _marco.height) }, WHITE);
	}
	
}
void modulo_matrix::select()
{
	bool nextTo;
	if (SelectedG == gems::None)
	{
		if (_selected.size() != 0)
		{
			_selected.clear();
		}
	}
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			if (CheckCollisionPointRec(GetMousePosition(), _matrix[i][f].rec))
			{
				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
				{
					//selecciona primera vez.
					if (SelectedG == gems::None)
					{
						if (_selected.size()!=0)
						{
							_selected.clear();
						}
						SelectedG = _matrix[i][f].type;
						_selected.push_back({ _matrix[i][f].rec.x,_matrix[i][f].rec.y });
						SelectedP = { static_cast<float>(i), static_cast<float>(f)};
						
					}
				}
				//selecciona resto de veces.
				else if (IsMouseButtonDown(MOUSE_LEFT_BUTTON))
				{
					//para asegurar que selecciono uno que este en el marco del anterior.
					nextTo = nextToF(SelectedP,i,f);
					
					if (SelectedG == _matrix[i][f].type && nextTo)
					{
						for (int w = 0; w < static_cast<int>(_selected.size()); w++)
						{
							if (_matrix[i][f].rec.x == _selected[w].x && _matrix[i][f].rec.y == _selected[w].y)
							{
								//eliminar todo a continuacion...
								int x = _selected.size()-1;
								while (x != w)
								{
									_selected.pop_back();
									x--;
								}
								
							}
							else
							{
								_selected.push_back({ _matrix[i][f].rec.x,_matrix[i][f].rec.y });
								SelectedP = { static_cast<float>(i), static_cast<float>(f) };
								if (_matrix[i][f].rec.x == _selected[w].x && _matrix[i][f].rec.y == _selected[w].y)
								{
									//eliminar todo a continuacion...
									int x = _selected.size() - 1;
									while (x != w)
									{
										_selected.pop_back();
										x--;
									}

								}
							}
						}
					}
				}
				else if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
				{
					
					if (static_cast<int>(_selected.size()) >= cuantitis::logic::minSelect)
					{
						cuantitis::logic::maxTurns--;
						cuantitis::rpg::damageType[static_cast<int>(SelectedG)] += _selected.size();
					}
					SelectedG = gems::None;
				}
			}
			if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON))
			{
				
				if (static_cast<int>(_selected.size())>=cuantitis::logic::minSelect)
				{ 
					//explotan. y deseleccionan.
					for (int j = 0; j < static_cast<int>(_selected.size()); j++)
					{
						if (_selected[j].x == _matrix[i][f].rec.x && _selected[j].y == _matrix[i][f].rec.y)
						{
							_matrix[i][f].type = gems::None; // se deselecciona despues por ser none.
							PlaySound(breakBroak);

						}
					}
				}
				else
				{
					_selected.clear();
				}
				
			}
		}
	}
}
void modulo_matrix::updateGem()
{
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			for (int w = 0; w < static_cast<int>(_selected.size()); w++)
			{
				if (_selected[w].x == _matrix[i][f].rec.x && _selected[w].y == _matrix[i][f].rec.y)
				{
					_matrix[i][f].color = giveColorLight(_matrix[i][f].type);
				}
				else
				{
					_matrix[i][f].color = giveColor(_matrix[i][f].type);
				}
			}
		}
	}
}
void modulo_matrix::destroyBlocks()
{
	Int2 selector = { 0,0 };
	bool matrixToErased[cuantitis::matrix::columns][cuantitis::matrix::rows];
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			matrixToErased[i][f] = false;
		}
	}
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			selector = { i,f };
			if (i+2< cuantitis::matrix::columns)
			{
				if (_matrix[i][f].type== _matrix[i+1][f].type && _matrix[i + 1][f].type == _matrix[i + 2][f].type)
				{
					//las 3 primeras son iguales por lo tanto se rompen.
					selector = { i + 3,f };
					while (nextSame(selector,i,f))
					{
						selector.x++;
					}
					for (int w = i; w < selector.x; w++)
					{
						if ((_matrix[i][f].type)> static_cast <gems>(0))
						{
							cuantitis::rpg::damageType[static_cast<int>(_matrix[i][f].type)]++;
							PlaySound(breakBroak);
						}
						if (w<cuantitis::matrix::columns)
						{
							if (w>=0&&f>=0) //si no pongo este if, indiga que se puede acceder a -10 byts.
							{
								matrixToErased[w][f] = true;
							}
							
						}
					}
				}
			}
			selector = { i,f };
			if (f+2< cuantitis::matrix::rows)
			{
				if (i>=0&&f>=0)
				{
					if (_matrix[i][f].type == _matrix[i][f + 1].type && _matrix[i][f + 1].type == _matrix[i][f + 2].type)
					{
						//las 3 primeras son iguales por lo tanto se rompen.
						selector = { i,f + 3 };
						cuantitis::rpg::damageType[static_cast<int>(_matrix[i][f].type)]+=3;
						while (nextSame(selector, i, f))
						{
							selector.y++;
							//cuantitis::rpg::damageType[static_cast<int>(_matrix[i][f].type)]++;
						}
						for (int w = f; w < selector.y; w++)
						{
							matrixToErased[i][w] = true;
						}
					}
				}
			}
			selector = { i,f };
		}
	}
	for (int i = 0; i < cuantitis::matrix::columns; i++)
	{
		for (int f = 0; f < cuantitis::matrix::rows; f++)
		{
			if (matrixToErased[i][f]==true)
			{
				_matrix[i][f].type = gems::None;
			}
		}
	}
}
bool modulo_matrix::nextSame(Int2 selector,int i,int f) {
	if (i != selector.x)
	{
		if (selector.x == cuantitis::matrix::columns || _matrix[selector.x - 1][selector.y].type != _matrix[selector.x][selector.y].type)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else if (f != selector.y)
	{
		if (selector.x == cuantitis::matrix::columns || _matrix[selector.x - 1][selector.y].type != _matrix[selector.x][selector.y].type)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
	
	
}

modulo_pvE::modulo_pvE()
{
	
	Vector2 colocator = { static_cast<float>(_texture.width / 10 * 1),static_cast<float>(_texture.height / 10 * 1) };
	colocator = { static_cast<float>(GetScreenWidth() / 10 * 1),static_cast<float>(GetScreenHeight() / 10 * 9) };
	TxBackB = LoadTexture("res/assets/botonBack.png");
	TxBackB.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 6);
	TxBackB.height = static_cast<int>(static_cast<float>(GetScreenWidth()) / 10);
	backB = new buton(0, colocator, TxBackB, 1);
	personaje_select = 0;
	_texture = LoadTexture("res/assets/FondoGameplay.png");
	_texture.width = GetScreenWidth() / 2;
	_texture.height = GetScreenHeight() / 2;
	_rec = { 0, 0, static_cast<float>(GetScreenWidth()/2), static_cast<float>(GetScreenHeight()/2) };
	_rec.x = _rec.width / 2;
	_rec.y = 0;
	for (int i = 0; i < static_cast<int>(damage::cuantitis); i++)
	{
		damageRecs[i] = { 0, static_cast<float>(i * GetScreenHeight()/60),static_cast<float>(GetScreenWidth()/20),static_cast<float>(GetScreenHeight() / 120) };
		personajes[i] = new personaje(i,_rec);
	}
	_enemys = new enemy(0, _rec);
}
bool modulo_pvE::gameOver()
{
	if (_enemys->getLife()<=0||cuantitis::logic::maxTurns<=0)
	{
		return true;
	}
	if (personajes[1]->getLife()<=0&& personajes[2]->getLife() <= 0&& personajes[3]->getLife() <= 0, personajes[4]->getLife() <= 0)
	{
		return true;
	}
	return false;
}
modulo_pvE::~modulo_pvE()
{
	UnloadTexture(_texture);
	delete _enemys;
	for (int i = 0; i < static_cast<int>(damage::cuantitis); i++)
	{
		delete personajes[i];
	}
	delete backB;
	UnloadTexture(TxBackB);
}
void modulo_pvE::draw()
{
	backB->drawButon(TxBackB);
	DrawTexture(_texture, _texture.width / 2, 0, WHITE);
	int dif = 30;
	for (int i = 1; i < static_cast<int>(damage::cuantitis); i++)
	{
		switch (i)
		{
		case static_cast<int>(damage::None):
			break;
			case static_cast<int>(damage::sword) :
				DrawText("Mele", 0, i * dif+20, 10, RED);
			break;
		case static_cast<int>(damage::magic):
				DrawText("Magic", 0, i * dif+20, 10, RED);
			break;
		case static_cast<int>(damage::bow):
				DrawText("Aim", 0, i * dif+20, 10, RED);
			break;
		case static_cast<int>(damage::shield):
				DrawText("Shield", 0, i * dif+20, 10, RED);
			break;
		case static_cast<int>(damage::cuantitis):
			break;
		default:
			break;
		}
		DrawRectangleLines(static_cast<int>(damageRecs[i].x), dif*i+30, static_cast<int>(damageRecs[i].width), static_cast<int>(damageRecs[i].height),GREEN);
		DrawRectangle(static_cast<int>(damageRecs[i].x), dif * i + 30, static_cast<int>(damageRecs[i].width / cuantitis::rpg::damageTypeTotal * cuantitis::rpg::damageType[i]), static_cast<int>(damageRecs[i].height), giveColor(static_cast<gems>(i)));
	}
	//pjs
	for (int i = 1; i < static_cast<int>(damage::cuantitis); i++)
	{
		if (!personajes[i]==NULL)
		{
			personajes[i]->draw();
		}
	}
	_enemys->draw();
}
void modulo_pvE::update()
{
	if (backB->actualizar())
		control::newCurrentScene = Scene::lvls;
	for (int i = 0; i < static_cast<int>(damage::cuantitis); i++)
	{
		personajes[i]->update();
		_enemys->update();
		_enemys->atack(personajes[GetRandomValue(1, static_cast<int>(damage::cuantitis)-1)]);
		if (cuantitis::rpg::damageType[i] >= cuantitis::rpg::damageTypeTotal)
		{
			personajes[i]->atack(_enemys);
			cuantitis::rpg::damageType[i] = 0;
		}
	}
}
