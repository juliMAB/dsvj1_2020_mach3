#include "generalitiesGameplay.h"
namespace cuantitis
{
	namespace matrix
	{
		gems diferentGems = gems::cuantitis;
	}
	namespace rpg
	{
		int enemigs=1;
		int allies;
		int damageType[static_cast<int>(damage::cuantitis)] = {0};
		int charge;
		int damageTypeTotal = 50;
		int Enemigcharge;
	}
	namespace gem
	{
		int frames = 10;
		int states = 1;
	}
	namespace logic
	{
		int minSelect = 3;
		int maxTurns = 50;
	}
}
namespace timers
{
	int gemSpawns = 10;
}
namespace letters
{
	//int small = 5;
	//int medium = 10;
	//int big = 20;
}
Color giveColor(gems g)
{
	switch (g)
	{
	case gems::None:
		return BLACK;
		break;
	case gems::Ruby:
		return RED;
		break;
	case gems::Shapire:
		return BLUE;
		break;
	case gems::Esmeralda:
		return GREEN;
		break;
	case gems::Amatista:
		return VIOLET;
		break;
	default:
		return ORANGE;
		break;
	}
}

Color giveColorLight(gems g)
{
	switch (g)
	{
	case gems::None:
		return { 0, 0, 0, 155 };
		break;
	case gems::Ruby:
		return { 230, 41, 55, 155 };
		break;
	case gems::Shapire:
		return { 0, 121, 241, 155 };
		break;
	case gems::Esmeralda:
		return { 0, 228, 48, 155 };
		break;
	case gems::Amatista:
		return { 135, 60, 190, 155 };
		break;
	default:
		return { 255, 161, 0, 155 };
		break;
	}
}

bool nextToF(Vector2 SelectedP, int i, int f)
{
	if (SelectedP.x == i - 1)
	{
		if (SelectedP.y == f - 1)
		{
			return true;
		}
		else if (SelectedP.y == f)
		{
			return true;
		}
		else if (SelectedP.y == f + 1)
		{
			return true;
		}
		return false;
	}
	else if (SelectedP.x == i)
	{
		if (SelectedP.y == f - 1)
		{
			return true;
		}
		else if (SelectedP.y == f)
		{
			return true;
		}
		else if (SelectedP.y == f + 1)
		{
			return true;
		}
	}
	else if (SelectedP.x == i + 1)
	{
		if (SelectedP.y == f - 1)
		{
			return true;
		}
		else if (SelectedP.y == f)
		{
			return true;
		}
		else if (SelectedP.y == f + 1)
		{
			return true;
		}
		return false;
	}
	return false;
}
