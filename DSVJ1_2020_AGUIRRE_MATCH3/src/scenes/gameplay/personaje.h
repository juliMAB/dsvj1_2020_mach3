#ifndef PERSONAJE
#define PERSONAJE
#include "raylib.h"
#include "generalitiesGameplay.h"
class enemy;
class personaje
{
public:
		 personaje(int i,Rectangle rec);
		~personaje();
	void	  draw();
	void animation();
	void	update();
	void	atack(enemy* p);
	void   setLife(int value);
	int    getLife();

private:
	int			 _timer;
	doing		_action;
	bool		 _alive;
	int	   _timeToAtack;
	int			 _frame;
	int		_protection;
	int			_damage;
	int			  _mana;
	int			  _life;
	int		_resistence;
	int			 _debuf;
	int			   _buf;
	damage		  _type;
	Rectangle	_inside;
	Texture2D  _texture;
	int			 height;
	int			  width;
	Rectangle	   _rec;
	Rectangle	_recIni;
};
class enemy
{
public:
	enemy(int i, Rectangle rec);
	~enemy();
	void	  draw();
	void animation();
	void	update();
	void   setLife(int value);
	int    getLife();
	void	  kill();
	void atack(personaje* p);

private:
	int			 _timer;
	doing		_action;
	bool		 _alive;
	int	   _timeToAtack;
	int			 _frame;
	int			_damage;
	int			  _life;
	damage		  _type;
	Rectangle	_inside;
	Texture2D  _texture;
	int			 height;
	int			  width;
	Rectangle	   _rec;
	Rectangle	_recIni;
};

#endif // !PERSONAJE
