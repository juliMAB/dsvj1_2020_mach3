#ifndef MODULE
#define MODULE
#include "raylib.h"
#include "generalitiesGameplay.h"
#include "personaje.h"
#include "scenes/menu/buton.h"
#include <vector>
class modulo_pvE
{
public:
	 modulo_pvE();
	 bool gameOver();
	~modulo_pvE();
	void   draw();
	void update();
private:
	int  personaje_select;

	Rectangle		 _rec;
	Texture2D    _texture;
	personaje* personajes[static_cast<int>(damage::cuantitis)];
	Rectangle  damageRecs[static_cast<int>(damage::cuantitis)];
	enemy*		  _enemys;
	buton*			backB;
	Texture2D	  TxBackB;
};
class modulo_matrix
{
public:
	modulo_matrix();
	~modulo_matrix();
	void fallingGems();
	void spawnGemsTop();
	void correctFalling();
	void update();
	void clearMatrix();
	void inputs();
	void draw();
	void draw(bool a);
	void select();
	void updateGem();
	void destroyBlocks();
	bool nextSame(Int2	selector, int i, int f);

private:
	Gem _matrix[cuantitis::matrix::columns][cuantitis::matrix::rows];
	Rectangle	   _marco;
	Texture2D    _texture;
	Texture2D	_textureG;
	Rectangle		 _rec;
	float		gemHeight;
	float		 gemWidth;
	std::vector<Vector2> _selected;
	gems		SelectedG;
	Vector2		SelectedP;
	bool		 _falling;
	Sound	   breakBroak;
	Texture2D   _TextureF;

};
#endif // !MODULE
