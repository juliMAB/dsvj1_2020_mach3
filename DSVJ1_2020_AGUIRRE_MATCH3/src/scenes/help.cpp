#include "help.h"

help::help(Texture2D textura)
{
	Vector2 colocator = { static_cast<float>(_texture.width / 10 * 1),static_cast<float>(_texture.height / 10 * 1) };
	colocator = { static_cast<float>(GetScreenWidth() / 10 * 1),static_cast<float>(GetScreenHeight() / 10 * 9) };
	TxBackB = LoadTexture("res/assets/botonBack.png");
	TxBackB.width = static_cast<int>(static_cast<float>(GetScreenWidth()) / 6);
	TxBackB.height = static_cast<int>(static_cast<float>(GetScreenWidth()) / 10);
	backB = new buton(0, colocator, TxBackB, 1);
	Vector2 posB = { static_cast<float>(GetScreenWidth()/10*2),static_cast<float>(GetScreenHeight()/2) };
	_texture = textura;
}

void help::update()
{
	if (backB->actualizar())
		control::newCurrentScene = Scene::Menu;
}

void help::draw()
{
	_texture.width = static_cast<int>(GetScreenWidth());
	_texture.height = static_cast<int>(GetScreenHeight());
	DrawTexture(_texture, 0, 0, WHITE);
	backB->drawButon(TxBackB);
}

help::~help()
{
	UnloadTexture(_texture);
	UnloadTexture(TxBackB);
	delete backB;
}
