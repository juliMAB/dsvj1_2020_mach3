#include "menu.h"

menu::menu(Texture2D texture)
{
	float screenH5 = static_cast<float>(GetScreenHeight() / 5);
	float screenW5 = static_cast<float>(GetScreenWidth()  / 2);
	Vector2 croz = { 0,0 };
	_texture = texture;
	_textureButons = LoadTexture("res/assets/Botones.png");

	_textureButons.height = static_cast<float>(static_cast<float>(GetScreenHeight()) / 15.5f*2);
	_textureButons.width  = static_cast<float>(static_cast<float>(GetScreenWidth())  / 2.6f*2);
	_buton[static_cast<int>(butons::play)] = new buton(static_cast<int>(butons::play), { screenW5,screenH5*1 }, _textureButons,cuantitiesButons);
	_buton[static_cast<int>(butons::help)] = new buton(static_cast<int>(butons::help), { screenW5 ,screenH5*2 }, _textureButons, cuantitiesButons);
	_buton[static_cast<int>(butons::credits)] = new buton(static_cast<int>(butons::credits), { screenW5,screenH5*3 }, _textureButons, cuantitiesButons);
	_buton[static_cast<int>(butons::close)] = new buton(static_cast<int>(butons::close), { screenW5,screenH5 *4}, _textureButons, cuantitiesButons);

}

menu::~menu()
{
	for (int i = 0; i < cuantitiesButons; i++)
	{
		delete _buton[i];
	}
	UnloadTexture(_texture);
	UnloadTexture(_textureButons);
}

void menu::update()
{
	if (_buton[static_cast<int>(butons::play)]->actualizar()) 
	control::newCurrentScene = Scene::lvls;
	if (_buton[static_cast<int>(butons::help)]->actualizar()) 
	control::newCurrentScene = Scene::Help;
	if (_buton[static_cast<int>(butons::credits)]->actualizar()) {}
	//incoming.
	//control::newCurrentScene = Scene::Credits;
	if (_buton[static_cast<int>(butons::close)]->actualizar())
		control::gameOver = true;
	
#ifndef DEBUG
		if (IsKeyReleased(KEY_H))
		{
			std::cout << "updating menu";
		}
#endif // !DEBUG

}

void menu::drawMenu()
{
	_texture.height = GetScreenHeight();
	_texture.width = GetScreenWidth();
	DrawTexture(_texture, 0, 0, WHITE);
	for (int i = 0; i < cuantitiesButons; i++)
	{
		_buton[i]->drawButon(_textureButons);
	}
}

