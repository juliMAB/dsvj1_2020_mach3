#ifndef GAME_H
#define GAME_H

#include "raylib.h"
#include "scenes/menu.h"
#include "scenes/help.h"
#include "scenes/lvls.h"
#include "scenes/gameplay.h"
namespace match3
{
	class game
	{
	public:
		game();
		~game();
		void run();
		void init();
		void update();
		void draw();
		void deinit();
	private:
		menu* menu1;
		help* help1;
		lvls* lvls1;
		gameplay* gameplay1;
		Music music;
	};
}


#endif