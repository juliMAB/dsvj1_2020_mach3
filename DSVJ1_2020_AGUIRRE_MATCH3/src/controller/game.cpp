#include "game.h"
#include <list>

using namespace match3;

		game::game()
		{
			init();
		}

		game::~game()
		{
			deinit();
		}


		void game::run()
		{
			while (!WindowShouldClose() && !control::gameOver)
			{
				update();
				draw();
			}
			if (control::gameOver)  WindowShouldClose();
			
		}

		void game::init()
		{
			control::screen::height = 600;
			control::screen::width = 800;
			InitWindow(control::screen::width, control::screen::height,"StickFightersDeluxe");
			//ToggleFullscreen();
			SetTargetFPS(60);
			InitAudioDevice();
			SetExitKey(0);
			music = LoadMusicStream("res/assets/beat1.mp3");
			SetMusicVolume(music, 0.5f);
			control::gameOver = false;
			control::currentScene = Scene::Menu;
			control::newCurrentScene = control::currentScene;
			control::volumeFx = 1.0f;
			control::volumeMusic = 1.0f;
			menu1 = new menu(LoadTexture("res/assets/FondoMenu.png"));
			letters::small = GetScreenHeight() / 100;
			letters::medium = GetScreenHeight() / 50;
			letters::big = GetScreenHeight()	/ 25;
		}

		void game::update()
		{
			if (IsKeyPressed(KEY_ESCAPE))
			{
				control::gameOver = true;
			}
			if (control::newCurrentScene != control::currentScene) //me movi de escena, por lo tanto tengo de destruir la anterior y cargar la nueva.
			{
				if (control::currentScene == Scene::Game)
				{
					//menu::scenes::actual_menu_scenes_nueva = menu::scenes::menu_scenes::general;
				}
				switch (control::currentScene)
				{
				case Scene::Menu:
					delete menu1;
					break;
				case Scene::Game:
					delete gameplay1;
					break;
				case Scene::Help:
					delete help1;
					break;
				case Scene::Credits:
					//delete credits1;
					break;
				case Scene::lvls:
					delete lvls1;
					
					break;
				case Scene::shop:
					break;
				default:
					break;
				}
				control::currentScene = control::newCurrentScene;
				switch (control::currentScene)
				{
				case Scene::Menu:
					menu1 = new menu(LoadTexture("res/assets/FondoMenu.png"));
					break;
				case Scene::Game:
					gameplay1 = new gameplay();
					UnloadMusicStream(music);
					break;
				case Scene::Help:
					help1 = new help(LoadTexture("res/assets/FondoHelp.png"));
					break;
				case Scene::Credits:
					//credits1 = new help(LoadTexture("../res/assets/FondoHelp"));
					break;
				case Scene::lvls:
					lvls1 = new lvls(LoadTexture("res/assets/FondoHelp.png"));
					music = LoadMusicStream("res/assets/beat1.mp3");
					SetMusicVolume(music, 0.5f);
					break;
				case Scene::shop:
					break;
				default:
					break;
				}
			}
			switch (control::currentScene)
			{
			case Scene::Menu:
				menu1->update();
				if (!IsMusicPlaying(music))
				{
					PlayMusicStream(music);
				}
				else
				{
					UpdateMusicStream(music);
				}
				break;
			case Scene::Game:
				gameplay1->update();
				break;
			case Scene::Help:
				help1->update();
				if (!IsMusicPlaying(music))
				{
					PlayMusicStream(music);
				}
				else
				{
					UpdateMusicStream(music);
				}
				break;
			case Scene::Credits:
				break;
			case Scene::lvls:
				lvls1->update();
				if (!IsMusicPlaying(music))
				{
					PlayMusicStream(music);
				}
				else
				{
					UpdateMusicStream(music);
				}
				break;
			case Scene::shop:
				break;
			default:
				break;
			}
			if (control::gameOver==true)
			{
				switch (control::currentScene)
				{
				case Scene::Menu:
					delete menu1;
					break;
				case Scene::Game:
					delete gameplay1;
					break;
				case Scene::Help:
					delete help1;
					break;
				case Scene::Credits:
					//delete credits1;
					break;
				case Scene::lvls:
					delete lvls1;
					break;
				case Scene::shop:
					break;
				default:
					break;
				}
			}
		}

		void game::draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			if (!control::gameOver)
			{
				switch (control::currentScene)
				{
				case Scene::Menu:
					//if (!menu::cambioDeEsena)
					menu1->drawMenu();
					break;
				case Scene::Game:
					gameplay1->draw();
					break;
				case Scene::Help:
					help1->draw();
					break;
				case Scene::Credits:
					break;
				case Scene::lvls:
					lvls1->draw();
					break;
				case Scene::shop:
					break;
				default:
					break;
				}
			}
			

			EndDrawing();
		}

		void game::deinit()
		{
			CloseWindow();
		}

	

		void deinitActualEscene(Scene currentScene)
		{
			switch (currentScene)
			{
			case Scene::Menu:
				//menu::deinit();
				break;
			case Scene::Game:
				//gameplay::deinit();
				break;
			}
		}
		